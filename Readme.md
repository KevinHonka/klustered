# Breaks for Klustered

This repo contains my breaks for this [klustered episode](https://www.youtube.com/watch?v=yck5Kr-bwxQ)

They are all run via Ansible and free to use however you please. Just keep in mind, they are not completely idempotent. Some things will break when they are run multiple times and backups will be destroyed.